# Symphony3 File Watcher

A utility application that watches for XML file changes in a specified folder and copies the changed files from source to destination folders.

## Installation
```
npm install -g sym3-fw
```

## Usage

```
sym3-fw path/to/source path/to/destination
```

> Note: The `path/to/source` and `path/to/destination` must be valid paths in the system.