#!/usr/bin/env node

const chokidar = require('chokidar');
const fse = require('fs-extra');
const path = require('path');

const source = path.resolve(process.argv[2]);
const destination = path.resolve(process.argv[3]);

// Initialize watcher.
const watcher = chokidar.watch(source, {
  ignored: /(^|[\/\\])\../, // ignore dotfiles
  persistent: true
});

const copyToDestination = (fullFilePath) => {
  let copyPath = destination + fullFilePath.replace(source, '');

  fse.copySync(fullFilePath, copyPath);
};

// Add event listeners.
watcher
  .on('add', (path) => {
    if (path.endsWith('.xml')) {
      copyToDestination(path);
    }
  })
  .on('change', (path) => {
    if (path.endsWith('.xml')) {
      copyToDestination(path);
    }
  });
